package telecom_Project.add_Customer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Read_Excel;

import org.openqa.selenium.JavascriptExecutor;

public class Add_Customer {
	
	ArrayList element = new ArrayList();
	
	//static Boolean a=false;
	 static int i=1;
	 static int a=0;
	
	public static void backgroundcheck(WebDriver driver) throws InterruptedException, IOException {
		
		a++;
		
		String  backgroundcheck="done";
		if(backgroundcheck.equalsIgnoreCase(Read_Excel.cellContent(a, 0, "C:\\Ritesh_Mahajan", "Telecom_Project.xlsx", "RadioButton"))){
			
			Thread.sleep(5000);
			JavascriptExecutor js = (JavascriptExecutor)driver;
			
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
			WebElement background= driver.findElement(By.xpath("//input[@type='radio' and @id='done']"));
			js.executeScript("arguments[0].click();", background);
			//System.out.println(background.isEnabled());
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//WebDriverWait wait = new WebDriverWait(driver,60);
			//WebElement background=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='radio' and @id='done']")));
			//background.click();
			
			
		}
		else {
			
JavascriptExecutor js = (JavascriptExecutor)driver;
			
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
			WebElement background= driver.findElement(By.xpath("//input[@type='radio' and @id='pending']"));
			js.executeScript("arguments[0].click()",background);
		}
			
			
		}
		
		public static void textboxes(WebDriver driver) throws IOException {
		List <WebElement> textbox = driver.findElements(By.xpath("//input[@type='text']"));
		int j=0;i++;
		for (WebElement content : textbox) {
			content.sendKeys(Read_Excel.cellContent(i, j, "C:\\Ritesh_Mahajan", "Telecom_Project.xlsx", "InputText"));
			j++;
		}
		
		WebElement address= driver.findElement(By.xpath("//textarea[@name='addr']"));
	    address.sendKeys(Read_Excel.cellContent(i, j, "C:\\Ritesh_Mahajan", "Telecom_Project.xlsx", "InputText"));
	    j++;
	    WebElement email= driver.findElement(By.xpath("//input[@type='email']"));
	    email.sendKeys(Read_Excel.cellContent(i, j, "C:\\Ritesh_Mahajan", "Telecom_Project.xlsx", "InputText"));
	    WebElement submit= driver.findElement(By.xpath("//input[@value='Submit']"));
	    submit.click();
	    WebElement customerid= driver.findElement(By.xpath("//h3"));
	    System.out.println(customerid.getText());
	}  

}
