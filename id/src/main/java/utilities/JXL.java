package utilities;
import page_classes.*;

import jxl.Workbook;
import jxl.format.CellFormat;
import jxl.format.UnderlineStyle;
import jxl.write.*;
import jxl.write.Number;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
public class JXL {
	//public static Read_Excel l =new Read_Excel();
	
	static JXL j=new JXL();
	
	 private static final String EXCEL_FILE_LOCATION = "C:\\Ritesh_Mahajan\\MyFirstExcel.xls";

	public  static void main(String[] args) {
		   WritableWorkbook myFirstWbook = null;
	        try {

	            myFirstWbook = Workbook.createWorkbook(new File(EXCEL_FILE_LOCATION));

	            // create an Excel sheet
	            WritableSheet excelSheet = myFirstWbook.createSheet("Sheet 1", 0);

	            // add something into the Excel sheet
	            //Label label =);
	            excelSheet.addCell( new jxl.write.Label(0, 0, Read_Excel.cellContent(1, 0, "C:\\Ritesh_Mahajan", "Ritesh.xlsx", "Dropdown"),j.getCellFormatByCondition(true)));
	           /* int i=9;
	            for(int a=0;a<1;a++) {
	            	
	            	for(int b=1;b<5;b+=2) {
	            
	            if(i>8) {

	            Number number = new Number(b, a, i);
	            excelSheet.addCell(number);
	            j.createFormatCellStatus(true);
	            
	            
	            }
	            
	            else
	            {
	            	int c=b+1;
	            	
	            	Number number = new Number(c, a, i);
		            excelSheet.addCell(number);
		            
	            }
	            	}
	            }*/
	            /*label = new Label(1, 0, "Result");
	            excelSheet.addCell(label);

	            label = new Label(1, 1, "Passed");
	            excelSheet.addCell(label);

	            number = new Number(0, 2, 2);
	            excelSheet.addCell(number);

	            label = new Label(1, 2, "Passed 2");
	            excelSheet.addCell(label);
*/
	            myFirstWbook.write();


	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (WriteException e) {
	            e.printStackTrace();
	        } finally {

	            if (myFirstWbook != null) {
	                try {
	                    myFirstWbook.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                } catch (WriteException e) {
	                    e.printStackTrace();
	                }
	            }


	        }
		
		
	}
	

    public WritableCellFormat createFormatCellStatus(boolean b) throws WriteException{
        jxl.format.Colour colour = (b == true) ? Colour.GREEN : Colour.RED;
        WritableFont wfontStatus = new WritableFont(WritableFont.createFont("Arial"), WritableFont.DEFAULT_POINT_SIZE, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, colour);
        WritableCellFormat fCellstatus = new WritableCellFormat(wfontStatus);

        fCellstatus.setWrap(true);
        fCellstatus.setAlignment(jxl.format.Alignment.CENTRE);
        fCellstatus.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        fCellstatus.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.MEDIUM, jxl.format.Colour.BLUE2);
        return fCellstatus;
    }
    
    public  WritableCellFormat getCellFormatByCondition(boolean condition) throws WriteException {
    	boolean b=true;
    	WritableFont wfontStatus;
        if(true){
        	 wfontStatus = new WritableFont(WritableFont.createFont("Arial"), WritableFont.DEFAULT_POINT_SIZE, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.GREEN);
        }else{
        	 wfontStatus = new WritableFont(WritableFont.createFont("Arial"), WritableFont.DEFAULT_POINT_SIZE, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.RED);
        }

        WritableCellFormat result = new WritableCellFormat(wfontStatus);
        result.setBackground(Colour.PINK);
        result .setWrap(true);
        result .setAlignment(jxl.format.Alignment.CENTRE);
        result .setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        result .setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.MEDIUM, jxl.format.Colour.BLUE2);
        return result;
        
        
    }

	 
	 

	
	
}
