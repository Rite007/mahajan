package utilities;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import driver.*;



public class Screenshot {
	
	static int i=1;
	
	public static void screen(WebDriver driver) throws IOException
	{
		
	
	TakesScreenshot scrShot =((TakesScreenshot)driver);
	
	File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
	
	File DestFile=new File(Read_Excel.cellContent(i, 0, "C:\\Ritesh_Mahajan", "Ritesh.xlsx", "Screenshot"));
	
	i++;
	
	FileUtils.copyFile(SrcFile, DestFile);

	
}
}
