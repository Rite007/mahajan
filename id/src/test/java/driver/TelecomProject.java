package driver;

import org.testng.annotations.Test;

import telecom_Project.*;
import telecom_Project.MoveToHomePage;
import telecom_Project.add_Customer.Add_Customer;
import telecom_Project.add_Customer.addCustomerhyperLink;
import telecom_Project.add_Customer.policy_Number;
import utilities.Write_Excel;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



 

  public class TelecomProject {
  	
  	static WebDriver driver;

  	
  	public  void  DriverChrome(WebDriver driver)
  	{
  	   System.setProperty("webdriver.chrome.driver","C:\\Selenium\\chromedriver.exe");	
  	   this.driver=new ChromeDriver();
  	 
  	  
  	 } 
  	
  public static void url() {
  	
  	driver.manage().window().maximize();
      driver.get("http://www.demo.guru99.com/v4/");
  }
  @Test(invocationCount=10)
  public static void testhomepage() throws InterruptedException, IOException, InvalidFormatException {
  	
  	TelecomProject telecom=new TelecomProject();
  	telecom.DriverChrome(driver);
  	TelecomProject.url();
  	MoveToHomePage.moveToHomePage(driver);
  	addCustomerhyperLink.clickhyperlink(driver);
  	Add_Customer.backgroundcheck(driver);
  	Add_Customer.textboxes(driver);
  	policy_Number.policyNumber(driver);
  	Write_Excel.idname(driver);
  	driver.close();
  	
  }
     
  }


